configuration {
    font:                           "JetBrains Mono bold 9";
    show-icons:                     true;
    drun-display-format:            "{name}";
    fullscreen:                     false;
    threads:                        0;
    scroll-method:                  0;
    disable-history:                false;
}

* {
    background:                     #0D1918;
    background-color:               #0D1918;
    background-entry:               #0D1918;
    background-alt:                 #0D1918;
    foreground:                     #e6e5df;
    foreground-selected:            #e6e5df;
    urgent:                         #0D1918;
    urgent-selected:                #0D1918;
    border-radius:                  15px;
}

window {
    transparency:                   "real";
    background-color:               @background;
    text-color:                     @foreground;
    width:							1140;
	height:                         85;
    location:                       north;
    x-offset:                       0;
    y-offset:                       203;
    border-radius:                  0px 0px 0px 0px;
}

prompt {
    enabled: false;
}


inputbar {
children: 						[ entry ];
    background-color:               #925F38;
    text-color:                     #0D1918;
    expand:                         false;
    margin:                         0% 30% 0% 30%;
    padding:                        0.2%;
}


entry {
    background-color:               #925F38;
    text-color:                     #0D1918;
    placeholder-color:              #0D1918;
    expand:                         true;
    horizontal-align:               0.5;
    placeholder:                    "Search applications";
    blink:                          true;
    padding:                        -0% 0% 0% 0%;
}

case-indicator {
    background-color:               @background;
    text-color:                     @foreground;
    spacing:                        2;
}


listview {
    background-color:               @background;
    columns:                        10;
    lines:	                        1;
    spacing:                        4px;
    cycle:                          false;
    dynamic:                        true;
    layout:                         vertical;
}

mainbox {
    background-color:               @background-color;
    children:                       [ listview ];
    spacing:                        0px;
    padding:                        0px 0% 0px 0%;
}


element {
    background-color:               @background;
    text-color:                     @foreground;
    orientation:                    vertical;
    padding:                        5px 0px 5px 0px;
}

element-icon {
    size:                           3.5%;
    background-color:               @background;
    text-color:                     inherit;
    horizontal-align:               0.5;
}

element-text {
    expand:                         true;
    horizontal-align:               0.5;
    vertical-align:                 0.5;
    margin:                         5px 10px 0px 10px;
    background-color: @background;
    text-color:       inherit;
}

element normal.urgent,
element alternate.urgent {
    background-color:               @urgent;
    text-color:                     @foreground;
}

element normal.active,
element alternate.active {
    background-color:               @background-alt;
    text-color:                     @foreground;
}

element selected {
    border: 0% 0% 0.4% 0%;
    border-color: #C3B49D;
    background-color:               #0D1918;
    text-color:                     #925F38;
}

element selected.urgent {
    background-color:               @urgent-selected;
    text-color:                     @foreground;
}

element selected.active {
    background-color:               @background-alt;
    color:                          @foreground-selected;
}
