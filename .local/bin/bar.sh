#!/usr/bin/env bash

trap "killall lemonbar" SIGINT SIGTERM

. "/home/uriel/.colors/nord.sh"

#FONTS="-o 0 -f berry-9 -o 1 -f SijiPlus-8 -o 0 -f Twemoji-8"
FONTS="-f -spectrum-berry-medium-r-normal-sans-11-80-75-75-m-50-iso10646-1 -f -wuncon-siji-medium-r-normal--10-100-75-75-c-80-iso10646-1"
WIDTH=1580 # bar width
HEIGHT=30 # bar height
XOFF=12 # x offset
YOFF=9 # y offset
BBG=${background} # bar background color
BFG=${foreground}
BDR=${color8}

# Status constants
# Change these to modify bar behavior
DESKTOP_COUNT=10
BATTERY_10=98
BATTERY_9=90
BATTERY_8=80
BATTERY_7=70
BATTERY_6=60
BATTERY_5=50
BATTERY_4=40
BATTERY_3=30
BATTERY_2=20
BATTERY_1=10

# Sleep constnats
BATTERY_SLEEP=10
WIRELESS_SLEEP=10
DATE_SLEEP=5

#Colors
BLACK="%{F$color0}"
RED="%{F$color1}"
GREEN="%{F$color2}"
YELLOW="%{F$color3}"
BLUE="%{F$color4}"
MAGENTA="%{F$color5}"
CYAN="%{F$color6}"
WHITE="%{F$color7}"
GREY="%{F$color8}"
ORANGE="%{F$color16}"
CLR="%{B-}%{F-}"

# Formatting Strings
# I would reccomend not touching these :D
SEP=" ${GREY}|${CLR} "
SEP2="${GREY}|${CLR} "

# Glyphs used for both bars
GLYWIN=$(echo -e "\ue1d7")
GLYLIGHT1=$(echo -e "\ue025")
GLYLIGHT2=$(echo -e "\ue023")
GLYLIGHT3=$(echo -e "\ue024")
GLYLIGHT4=$(echo -e "\ue022")
GLYTIME=$(echo -e "\ue017")
GLYVOL1=$(echo -e "\ue04e")
GLYVOL2=$(echo -e "\ue050")
GLYVOL3=$(echo -e "\ue050")
GLYVOL4=$(echo -e "\ue05d")
GLYVOL5=$(echo -e "\ue05d")
GLYVOLM=$(echo -e "\ue04f")
GLYBAT1=$(echo -e "\ue242")
GLYBAT2=$(echo -e "\ue243")
GLYBAT3=$(echo -e "\ue244")
GLYBAT4=$(echo -e "\ue245")
GLYBAT5=$(echo -e "\ue246")
GLYBAT6=$(echo -e "\ue247")
GLYBAT7=$(echo -e "\ue248")
GLYBAT8=$(echo -e "\ue249")
GLYBAT9=$(echo -e "\ue24a")
GLYBAT10=$(echo -e "\ue24b")
GLYBATCHG=$(echo -e "\ue23a")
GLYWLAN1=$(echo -e "\ue218")
GLYWLAN2=$(echo -e "\ue219")
GLYWLAN3=$(echo -e "\ue21a")
GLYWLAN4=$(echo -e "\ue21a")
GLYWLAN5=$(echo -e "\ue21a")
GLYWS1=$(echo -e "\ue1a0")
GLYWS2=$(echo -e "\ue1a8")
GLYWS3=$(echo -e "\ue1d5")
GLYWS4=$(echo -e "\ue1ec")
GLYWS5=$(echo -e "\ue1e0")
GLYWS6=$(echo -e "\ue1a7")
GLYWS7=$(echo -e "\ue05c")
GLYWS8=$(echo -e "\ue1f5")
GLYWS9=$(echo -e "\ue1da")
GLYWS10=$(echo -e "\ue027")
GLYLYTH=$(echo -e "\ue26b")
GLYLYTV=$(echo -e "\ue004")
GLYLYTM=$(echo -e "\ue000")
GLYLYTG=$(echo -e "\ue005")

PANEL_FIFO=/tmp/panel-fifo
OPTIONS="-d ${FONTS} -g ${WIDTH}x${HEIGHT}+${XOFF}+${YOFF} -B ${BBG} -F ${BFG} -u 0 -a 25"

[ -e "${PANEL_FIFO}" ] && rm "${PANEL_FIFO}"
mkfifo "${PANEL_FIFO}"


clock()
{
	while true; do
		local clock="$(date +'%I:%M %p')"
		echo "CLOCK ${MAGENTA}${GLYTIME}${CLR} %{A1:notify-send -u normal -t 3000 ' $(date '+%A, %B %d %Y')':}${clock}%{A}"

		sleep ${DATE_SLEEP}
	done
}

clock > "${PANEL_FIFO}" &

echo "" | lemonbar -p -g 1584x36+10+6 -B "${BDR}" -d -n "bg" &
sleep 0.2
#echo "" | lemonbar -p -g 1576x28+10+11 -B "${BDR}" -d &

while read -r line; do
	case $line in
		CLOCK*)
			fn_time="${line#CLOCK }"
			;;
	esac
	printf "%s\n" "%{l}  "
done < "${PANEL_FIFO}" | lemonbar ${OPTIONS} | sh > /dev/null
