" Special
let wallpaper  = "/home/uriel/Imágenes/jzl9w.jpg"
let background = "#0D1918"
let foreground = "#e6e5df"
let cursor     = "#e6e5df"

" Colors
let color0  = "#0D1918"
let color1  = "#925F38"
let color2  = "#8E7552"
let color3  = "#7E807B"
let color4  = "#A28D6B"
let color5  = "#CEA172"
let color6  = "#C3B49D"
let color7  = "#e6e5df"
let color8  = "#a1a09c"
let color9  = "#925F38"
let color10 = "#8E7552"
let color11 = "#7E807B"
let color12 = "#A28D6B"
let color13 = "#CEA172"
let color14 = "#C3B49D"
let color15 = "#e6e5df"
