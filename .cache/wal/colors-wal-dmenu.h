static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#e6e5df", "#0D1918" },
	[SchemeSel] = { "#e6e5df", "#925F38" },
	[SchemeOut] = { "#e6e5df", "#C3B49D" },
};
