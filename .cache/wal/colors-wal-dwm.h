static const char norm_fg[] = "#e6e5df";
static const char norm_bg[] = "#0D1918";
static const char norm_border[] = "#a1a09c";

static const char sel_fg[] = "#e6e5df";
static const char sel_bg[] = "#8E7552";
static const char sel_border[] = "#e6e5df";

static const char urg_fg[] = "#e6e5df";
static const char urg_bg[] = "#925F38";
static const char urg_border[] = "#925F38";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
