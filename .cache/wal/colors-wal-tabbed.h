static const char* selbgcolor   = "#0D1918";
static const char* selfgcolor   = "#e6e5df";
static const char* normbgcolor  = "#8E7552";
static const char* normfgcolor  = "#e6e5df";
static const char* urgbgcolor   = "#925F38";
static const char* urgfgcolor   = "#e6e5df";
