const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#0D1918", /* black   */
  [1] = "#925F38", /* red     */
  [2] = "#8E7552", /* green   */
  [3] = "#7E807B", /* yellow  */
  [4] = "#A28D6B", /* blue    */
  [5] = "#CEA172", /* magenta */
  [6] = "#C3B49D", /* cyan    */
  [7] = "#e6e5df", /* white   */

  /* 8 bright colors */
  [8]  = "#a1a09c",  /* black   */
  [9]  = "#925F38",  /* red     */
  [10] = "#8E7552", /* green   */
  [11] = "#7E807B", /* yellow  */
  [12] = "#A28D6B", /* blue    */
  [13] = "#CEA172", /* magenta */
  [14] = "#C3B49D", /* cyan    */
  [15] = "#e6e5df", /* white   */

  /* special colors */
  [256] = "#0D1918", /* background */
  [257] = "#e6e5df", /* foreground */
  [258] = "#e6e5df",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
